# a) solution
After running 
```
grep . /sys/devices/system/cpu/cpu0/cache/index*/*
```
We get 
* index0 (data): size: 32k; cache line: 64
* index1 (instruction): size: 32k; cache line: 64
* index2: size: 256k; cache line: 64
* index3: size: 6144k; cache line: 64
* index4: size: 131072k; cache line: 64

# b) solution
The drop approximately match the cache size. Only the transition at cache size is sharp, because this will result in frequently accessing data in different higher level caches with random jumping.

# c) solution
The transition is less sharp, because the addresses are accessed in order, with less transition between high level caches.

# d) solution
The sharp transition are less than a), the performance is between the above two cases. When N is large, two much cache misses limit its performance. For random jump, too much jump between higher level caches limits the performance.

![performance curve](https://gitlab.ethz.ch/yunkao/hpc_hw/-/blob/master/ex01/solutions/Question3/3_cache_size/results.png)