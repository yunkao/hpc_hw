# a) solution:
* Row major is faster, because row major matrix read data sequentially, while column major matrix have less spacial locality, which result in more cache miss and more time cost.
* As the matrix dimension increase, the relative speedup improve rapidly. This is because as dimension increases, the distance between addresses of neighbors of column major matrixes increase, which cause more cache miss in higher cache levels.
# b) solution:
* Applying blockwise transpose results in speedup. As is shown in the figure, the most speedup is achieved when the blocksize is 8. This is possible to be determined by the cache size of cpu (64bit). When the blocksize match the cache size, the least cache miss is achieved.

![transpose time](https://gitlab.ethz.ch/yunkao/hpc_hw/-/blob/master/ex01/solutions/Question2/2_cache_matrices/transpose.png)
# c) solution:
* Also applying blockwise transpose results in speedup. As is shown in the figure, the most speedup is achieved when the blocksize is 8-16. 
* Besides, columnwise block is even faster, because this further improves spatial locality (visit addresses more in the same cache) in each block when doing the multiplication, as is explained in a).

![matrix_matrix time](https://gitlab.ethz.ch/yunkao/hpc_hw/-/blob/master/ex01/solutions/Question2/2_cache_matrices/matrix_matrix.png)